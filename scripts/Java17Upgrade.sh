#!/bin/bash

echo "Installing Requirements"

python3 -m ensurepip --default-pip
python3 -m pip install -r $PWD/requirements.txt

yum install -y sudo

curl -s https://raw.githubusercontent.com/lindell/multi-gitter/master/install.sh | sh

echo "Running dockerfileUpgrade.py multi-gitter"

multi-gitter run "python3 $PWD/scripts/dockerfileUpgrade.py" --config=config.yaml

echo "Running gitlab-ciUpgrade.py multi-gitter"

multi-gitter run "python3 $PWD/scripts/gitlab-ciUpgrade.py" --config=config.yaml

echo "Running readMeUpgrade.py multi-gitter"

multi-gitter run "python3 $PWD/scripts/readMeUpgrade.py" --config=config.yaml

echo "Running POMUpgrade.py multi-gitter"

multi-gitter run "python3 $PWD/scripts/POMUpgrade.py" --config=config.yaml

echo "Finished"
