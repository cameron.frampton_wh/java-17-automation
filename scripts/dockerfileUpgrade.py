""""
If filename == .gitlab-ci.yml

Get line that contains FROM statement

For ease of use hardcode location of new image

If line contains trading-newrelic:ol7-java11 replace with new location
"""
import os
import ruamel.yaml as yaml

dict_ecr_images = {
    "trading-ol7": "ecr...",
    "trading-ol7-java11": "ecr..java11",
    "trading-ol7-java17": "ecr..java17"
}

yaml = yaml.YAML()


def return_docker_file(file_name):
    dlist = []
    for dirpath, dirnames, filenames in os.walk("."):
        for filename in [f for f in filenames if f.endswith("Dockerfile")]:
            dlist.append(os.path.join(dirpath, filename))

    for file in dlist:
        filename: str = file
        print(filename)
        return filename


def removeDockerRegistry(dockerfile):
    # replace docker-registery with ecr
    new_image = '663553139883.dkr.ecr.eu-west-1.amazonaws.com/platform/docker-images/trading-new-relic:ol7-java17'
    nexus_path = 'nexus-aws.dtc.prod.williamhill.plc/trading/trading-new-relic:ol7'

    # Load
    handle = open(dockerfile, "r")
    content = handle.read()
    content = content.replace(nexus_path, new_image)
    handle.close()

    # Not the most resilient as if pod gets bapped then we will get an empty Docker file

    # OVERWRITE
    handle = open(dockerfile, "w")
    handle.write(content)
    handle.close()


if __name__ == "__main__":
    file_name = return_docker_file("Dockerfile")
    removeDockerRegistry(file_name)
