import os


def return_readme_file(file_name):
    dlist = []
    for dirpath, dirnames, filenames in os.walk("."):
        for filename in [f for f in filenames if f.endswith("README.md")]:
            dlist.append(os.path.join(dirpath, filename))

    for file in dlist:
        filename: str = file
        print(filename)
        return filename


def add_line_to_readme(file_path, line_to_add):
    with open(file_path, 'r') as file:
        content = file.read()

    with open(file_path, 'w') as file:
        file.write(f"{line_to_add}\n{content}")


if __name__ == "__main__":
    file_name = return_readme_file('README.md')  # Replace with the actual path to your README.md file
    line_to_add = 'Automated Upgrade to Java 17'
    add_line_to_readme(file_name, line_to_add)
