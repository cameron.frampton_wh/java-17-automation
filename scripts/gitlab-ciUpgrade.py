import os

import yaml


def return_gitlab_file(file_name):
    dlist = []
    for dirpath, dirnames, filenames in os.walk("."):
        for filename in [f for f in filenames if f.endswith(".gitlab-ci.yml")]:
            dlist.append(os.path.join(dirpath, filename))

    for file in dlist:
        filename: str = file
        print(filename)
        return filename


def process_yaml(gitlab_file):
    with open(gitlab_file, 'r') as file:
        data = yaml.safe_load(file)

    data['include'] = [{'project': 'williamhillplc/trading/platform/gitlab-templates',
                        'file': '.gitlab-ci-maven-java-auto-template.yml'}]

    # Keep 'tech_STACK' and add 'ENABLE_SNAPSHOT'
    data['variables']['ENABLE_SNAPSHOT'] = 'true'

    # Keep only the 'include' part for project and file tags
    keys_to_delete = [
        "FF_GITLAB_REGISTRY_HELPER_IMAGE",
        "KUBERNETES_MEMORY_REQUEST",
        "KUBERNETES_MEMORY_LIMIT",
        "GIT_STRATEGY",
        "MAVEN_OPTS",
        "ECR_ENABLED"
    ]

    if "variables" in data:
        for key in keys_to_delete:
            data["variables"].pop(key, None)

    # Remove unnecessary sections
    sections_to_remove = [
        'build',
        'cache',
        'docker-aws-release-push',
        'docker-aws-snapshot-push',
        'stages',
        'tag-project',
    ]
    for section in sections_to_remove:
        if section in data:
            del data[section]

    with open(gitlab_file, 'w') as file_contents:
        yaml.safe_dump(data, file_contents)



if __name__ == "__main__":
    file_name = return_gitlab_file("gitlab-ci.yml")
    process_yaml(file_name)
